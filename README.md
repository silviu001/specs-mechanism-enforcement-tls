# SPECS Enforcement -- HAProxy #
SPECS Enforcement HAProxy is in charge with the configuration of the PROXY Terminator for secured channels communication.

# Table of contents #
[TOC]

## Installation and usage ##

### Requirements ###

* haproxy >= 1.5.14
* systemd (for process management)

### Installation ###

These installation steps are compatible with mOS 4.0.x or OpenSUSE 13.1 (with SPECS repositories activated) environments.

* Install the requirements

```
#!bash

zypper install specs-enforcement-haproxy
```

### Usage ###

```
#!bash

# use the appropiate options to generate PROXY Terminator configuration file
/opt/specs-enforcement-haproxy/bin/tls-configurator --help
/opt/specs-enforcement-haproxy/bin/webpool-configurator --help
# use the appropiate flags to check if various configurations are not changed in PROXY Termiantor configuration file
/opt/specs-enforcement-haproxy/bin/tls-prober --help
/opt/specs-enforcement-haproxy/bin/webpool-prober --help
# use control flags to manage the PROXY Termiantor
/opt/specs-enforcement-haproxy/bin/proxy-controller
```

Very important:

* PROXY Terminator uses a self-signed certificate for TLS communication (current version doesn't support custom SSL certificates);

# NOTICE #

This product includes software developed at "Institute e-Austria, Timisoara", as part of the "SPECS - Secure Provisioning of Cloud Services based on SLA Management" research project (an EC FP7-ICT Grant, agreement 610795).

* http://www.specs-project.eu/
* http://www.ieat.ro/

*Developers:*

    Silviu Panica, silviu@solsys.ro / silviu.panica@e-uvt.ro

*Copyright:*

```
Copyright 2013-2015, Institute e-Austria, Timisoara, Romania
    http://www.ieat.ro/

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at:
    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```