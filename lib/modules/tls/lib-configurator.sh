#!/bin/bash

# Copyright 2013-2015, Institute e-Austria, Timisoara, Romania, http://ieat.ro/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Institute e-Austria, Timisoara", http://www.ieat.ro/ .
#
# Developers:
#  * Silviu Panica <silviu@solsys.ro>, <silviu.panica@e-uvt.ro>

## metric variables (do not edit)
metric_m3=0
metric_m3_plus=0
metric_m4=0
metric_m5=0
metric_m6=0
metric_m7=0
metric_m10=0
update_only=0
enforce=0
####

function tls_configurator_arguments_parser() {
	ARGS=$(getopt -o a,b,c,d,e,f,g,h,i,j:,k,l --longoptions 'm3,m3-plus,m4,m5,m6,m7,m10,help,tls-backend:,enforce,clean' -n "proxy-configurator.sh" -- "$@");
	if [[ $? -ne 0 || $# -eq 0 ]];then
		tls_configurator_helper
		exit 1
	fi
	eval set -- "${ARGS}"

	while true ; do
	    case "$1" in
	    	--enforce) update_only=1; enforce=1; shift ;
	    	;;
	    	--m3) metric_m3=1; shift;
	        ;;
	        --m3-plus) metric_m3_plus=1; shift;
	        ;;
	    	--m4) metric_m4=1; shift;
	        ;;
	        --m5) metric_m5=1; shift;
	        ;;
	        --m6) metric_m6=1; shift;
	        ;;
	        --m7) metric_m7=1; shift;
	        ;;
	        --m10) metric_m10=1; shift;
	        ;;
	        --tls-backend)
	        	shift;
	        	if [ -n "$1" ];then
	        		tls_backend=$1
	        		shift;
	        	fi
	        ;;
			--clean) rm -f ${_proxy_terminator_cert}; rm -f ${_proxy_terminator_config}; shift;
	        ;;
	    	--help) tls_configurator_helper; exit 0;
	        ;;
	        --) shift; break;
	        ;;
	        *) tls_configurator_helper; exit 1;
	        ;;
	    esac
	done
}

function tls_configurator_helper() {
	echo
	echo "   Usage for TLS Configurator module: $0 [options]"
	echo
	echo "      Options:"
	echo
	echo "            --m3      - enforce M3 Crypto Strength level < 7"
	echo "            --m3-plus - enforce M3 Crypto Strength level >= 7"
	echo "            --m4      - enforce M4 Forward Secrecy"
	echo "            --m5      - enforce M5 HTTP Strict Transport Security"
	echo "            --m6      - enforce M6 HTTP to HTTPS Redirection"
	echo "            --m7      - enforce M7 Secure Cookies Forced"
	echo "            --m10     - enforce M10 Certificate Pinning"
	echo
	echo "            --tls-backend {address}:{port} - TLS Backend connection"
	echo "            --clean   - remove configuration file and SSL certificate chain"
	echo
}

function tls_configurator_metric_cipher() {
	# M4 Forward secrecy
	# M3_plus Crypto Strength > 7
	# M3 Crypto Strength < 7
	if [ $# -ne 1 ];then
		echo "${_banner}::${0}: missing metric name!"
		exit 1
	fi
	eval _t=\$metric_${1,,}
	eval _tls_f=\$_tls_metric_${1,,}
	if [ ! -f ${_proxy_terminator_config} ];then
		echo "${_banner}::tls_configurator_metric_cipher: missing configuration file. Run first configuration initialization."
		exit 1
	fi
	if grep -q "`cat ${_proxy_configurator_templates}/${_tls_f}`" ${_proxy_terminator_config};then
		echo "${_banner}:: the TLS Enforce ${1} configuration already exists and matches the initial configuration"
		return
	else
		_tls_metric_temp=`cat ${_proxy_configurator_templates}/${_tls_f}`
		sed -i -e "s/ssl-default-bind-ciphers .*/${_tls_metric_temp}/g" ${_proxy_terminator_config}
	fi
}

function tls_configurator_metric_enforce_m5() {
	# HTTP Strict Transport Security
	local _head='frontend https-in'
	local _tail='##--end-frontend-https-in'
	local _mhead='#~tls-metric-m5'
	local _mtail='#~tls-metric-m5-end'
	local _file=${_proxy_configurator_templates}/${_tls_metric_m5}

if grep -q "${_mhead}" ${_proxy_terminator_config} && grep -q "${_mtail}" ${_proxy_terminator_config};then
		if proxy_configurator_compare_content_ph ${_mhead} ${_mtail} ${_file};then
			echo "${_banner}:: the TLS Enforce M5 configuration already exists and matches the initial configuration"
			return
		else
			proxy_configurator_replace_content_ph "${_mhead}" "${_mtail}" 0 ${_file} delete_old_entries
		fi
	else
		proxy_configurator_replace_content_ph "${_head}" "${_tail}" 4 ${_file}
	fi
}

function tls_configurator_metric_enforce_m6() {
	# HTTP to HTTPS redirection
	local _head='frontend http-in'
	local _tail='##--end-frontend-http-in'
	local _mhead='#~tls-metric-m6'
	local _mtail='#~tls-metric-m6-end'
	local _file=${_proxy_configurator_templates}/${_tls_metric_m6}

	if grep -q ${_mhead} ${_proxy_terminator_config} && grep -q ${_mtail} ${_proxy_terminator_config};then
		if proxy_configurator_compare_content_ph ${_mhead} ${_mtail} ${_file};then
			echo "${_banner}:: the TLS Enforce M6 configuration already exists and matches the initial configuration"
			return
		else
			proxy_configurator_replace_content_ph "${_mhead}" "${_mtail}" 0 ${_file} delete_old_entries
		fi
	else
		proxy_configurator_replace_content_ph "${_head}" "${_tail}" 4 ${_file}
	fi
}

function tls_configurator_metric_enforce_m7() {
	# Secure cookie forced
	local _head='frontend https-in'
	local _tail='##--end-frontend-https-in'
	local _mhead='#~tls-metric-m7'
	local _mtail='#~tls-metric-m7-end'
	local _file=${_proxy_configurator_templates}/${_tls_metric_m7}

	if grep -q "${_mhead}" ${_proxy_terminator_config} && grep -q "${_mtail}" ${_proxy_terminator_config};then
		if proxy_configurator_compare_content_ph ${_mhead} ${_mtail} ${_file};then
			echo "${_banner}:: the TLS Enforce M7 configuration already exists and matches the initial configuration"
			return
		else
			proxy_configurator_replace_content_ph "${_mhead}" "${_mtail}" 0 ${_file} delete_old_entries
		fi
	else
		proxy_configurator_replace_content_ph "${_head}" "${_tail}" 4 ${_file}
	fi
}

function tls_configurator_metric_enforce_m10() {
	# Certificate pinning
	# Generate the cert_pin using: openssl rsa -in /path/to/cert--key.pem -outform der -pubout | openssl dgst -sha256 -binary | base64
	local _head='frontend https-in'
	local _tail='##--end-frontend-https-in'
	local _mhead='#~tls-metric-m10'
	local _mtail='#~tls-metric-m10-end'
	local _file=${_proxy_configurator_templates}/${_tls_metric_m10}
	if grep -q "${_mhead}" ${_proxy_terminator_config} && grep -q "${_mtail}" ${_proxy_terminator_config};then
	if proxy_configurator_compare_content_ph ${_mhead} ${_mtail} ${_file} ${_proxy_terminator_cert};then
			echo "${_banner}:: the TLS Enforce M10 configuration already exists and matches the initial configuration"
			return
		else
			proxy_configurator_replace_content_ph "${_mhead}" "${_mtail}" 0 ${_file} delete_old_entries
			proxy_get_certificate_sha256 ${_proxy_terminator_cert} ${_proxy_terminator_config}
		fi
	else
		proxy_configurator_replace_content_ph "${_head}" "${_tail}" 4 ${_file}
		proxy_get_certificate_sha256 ${_proxy_terminator_cert} ${_proxy_terminator_config}
	fi
}

function tls_configurator_backend() {
	# TLS backend

	local _head='backend default-backend'
	local _tail='##--end-backend-default'
	local _mhead='#~proxy-backend-instances'
	local _mtail='#~proxy-backend-instances-end'
	local _file=/tmp/_tls_backend_temp.instances
	if [ $# -eq 1 ];then
		if proxy_check_backend ${1};then
		local _endpoint=${1}
		else
			exit 1
		fi
	else
		local _endpoint='127.0.0.1:81'
	fi

	if [ -f ${_file} ];then
		rm -f ${_file}
	fi
	echo ${_mhead} >> ${_file}
	echo -e "server proxy-backend-instance ${_endpoint} check"  >> ${_file}
	echo ${_mtail} >> ${_file}

	if grep -q "${_mhead}" ${_proxy_terminator_config} && grep -q "${_mtail}" ${_proxy_terminator_config};then
		if proxy_configurator_compare_content_ph ${_mhead} ${_mtail} ${_file};then
			return
		else
			proxy_configurator_replace_content_ph "${_mhead}" "${_mtail}" 0 ${_file} delete_old_entries
		fi
	else
		proxy_configurator_replace_content_ph "${_head}" "${_tail}" 2 ${_file}
	fi

	if [ -f ${_file} ];then
		rm -f ${_file}
	fi

}

##### main function to be called from outside #####

function tls_configurator_update() {
	if [ ${metric_m4} -eq 1 ];then
		tls_configurator_metric_cipher M4
	elif [ ${metric_m3_plus} -eq 1 ];then
		tls_configurator_metric_cipher M3_plus
	elif [ ${metric_m3} -eq 1 ];then
		tls_configurator_metric_cipher M3
	fi
	if [ ${metric_m6} -eq 1 ];then
		tls_configurator_metric_enforce_m6
	fi
	if [ ${metric_m5} -eq 1 ];then
		tls_configurator_metric_enforce_m5
	fi
	if [ ${metric_m7} -eq 1 ];then
		tls_configurator_metric_enforce_m7
	fi
	if [ ${metric_m10} -eq 1 ];then
		tls_configurator_metric_enforce_m10
	fi

	if [ ! -z "${tls_backend}" ];then
		tls_configurator_backend ${tls_backend}
	else
		tls_configurator_backend
	fi

}
