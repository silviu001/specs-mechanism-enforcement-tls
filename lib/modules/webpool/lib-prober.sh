#!/bin/bash

# Copyright 2013-2015, Institute e-Austria, Timisoara, Romania, http://ieat.ro/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Institute e-Austria, Timisoara", http://www.ieat.ro/ .
#
# Developers:
#  * Silviu Panica <silviu@solsys.ro>, <silviu.panica@e-uvt.ro>

_banner="[webpool-prober]"

## metric variables (do not edit)
metric_m1=0
metric_m1_value=1
metric_m2=0
metric_m2_value=1
####

_proxy_prober_monitoring_component="webpool-prober"
_proxy_prober_monitoring_object="webpool-prober_object"
_proxy_prober_monitoring_labels=("webpool-prober" "webpool-metrics")
_proxy_prober_monitoring_type="alert"
_proxy_prober_monitoring_data=("metric_m1=1")


################################

function webpool_prober_arguments_parser() {
	_nargs=$#
	ARGS=$(getopt -o a:b:c --longoptions 'm1:,m2:,help' -n "proxy-configurator.sh" -- "$@");
	if [[ $? -ne 0 && $# -ne 0 ]];then
		tls_prober_helper
		exit 1
	fi
	
	eval set -- "${ARGS}"
	
	while true ; do
	    case "$1" in
	    	--m1) metric_m1=1; 
	    		shift;
				if [ ${#} -gt 0 ];then
	        		metric_m1_value=${1} 
	        		shift;
	        	fi
	        ;;
	    	--m2) metric_m2=1; 
	    		shift;
				if [ ${#} -gt 0 ];then
	        		metric_m2_value=${1} 
	        		shift;
	        	fi
	        ;;
	        --help) webpool_prober_helper; exit 0;
	        ;;
	        --) shift; break;
	        ;;
	    esac
	done
	
	# if no parameter set then check the cache folder
	if [ ${_nargs} -eq 0 ];then
		if [ ! -d ${_proxy_prober_cache} ];then
			mkdir -p ${_proxy_prober_cache}
		fi
		_msr_list=$(ls ${_proxy_prober_cache})
		if [ ! -z ${_msr_list} ];then
			__m=(${_msr_list})
			if [ ${#__m{@}} -eq 0 ];then
				echo "${_banner} No measurements found in (${_proxy_prober_cache})."
				exit 1
			fi
			for ((i=0; i<${#__m[@]}; i++)) {
				case "${__m[i]}" in
					m1) 
					metric_m1_value=`cat ${_proxy_prober_cache}/m1`
					if [ -z ${metric_m1_value} ];then
						metric_m1_value=1
					fi
					metric_m1=1; 						
					shift;
			        ;;
			        m2)
			        metric_m2_value=`cat ${_proxy_prober_cache}/m2`
					if [ -z ${metric_m1_value} ];then
						metric_m2_value=1
					fi 
			        metric_m2=1; 
			        shift;
			        ;;
				esac
			}
		else
			echo "${_banner}: No measurements were configured to probe!"
			exit 1
		fi
	fi
	
	webpool_prober_check_metrics
}


function webpool_prober_helper() {
	echo
	echo "   Usage for TLS Prober module: $0 [options]"
	echo
	echo "      Options:"
	echo
	echo "            --m1 LoR   - check M1 Level of Redundancy"
	echo "            --m2 LoD   - check M2 Level of Diversity"
	echo
}

function webpool_prober_metric_m1() {
	local _banner="${_banner}: webpool_prober_metric_m1:"
	local _head='backend default-backend'
	local _tail='##--end-backend-default'
	local _mhead='#~proxy-backend-instances'
	local _mtail='#~proxy-backend-instances-end'

	if grep -q "${_mhead}" ${_proxy_terminator_config} && grep -q "${_mtail}" ${_proxy_terminator_config};then
		if ! grep -q 'server proxy-backend-instance-.*:.* check' ${_proxy_terminator_config};then
			echo "${_banner}: [ERROR] proxy backends are missing from PROXY Terminator configuration file. Maybe not configured for WebPool ?!"
			return 1
		else
			if [ `grep 'server proxy-backend-instance-.*:.* check' ${_proxy_terminator_config} | wc -l` -eq ${metric_m1_value} ];then
				echo "${_banner}: [OK] M1 LoR level is equal with the requested probe value (${metric_m1_value})"
				return 0
			else
				echo "${_banner}: [ERROR] M1 LoR level is below the requested probe value (${metric_m1_value})"
				return 1
			fi
		fi
	else
		echo "${_banner}: [ERROR] No backend entries could be found in PROXY Terminator configuration file."
		return 1
	fi
}

function webpool_prober_metric_m2() {
	local _banner="${_banner}: webpool_prober_metric_m2:"
	local _head='backend default-backend'
	local _tail='##--end-backend-default'
	local _mhead='#~proxy-backend-instances'
	local _mtail='#~proxy-backend-instances-end'
	
	if grep -q "${_mhead}" ${_proxy_terminator_config} && grep -q "${_mtail}" ${_proxy_terminator_config};then
		if ! grep -q 'server proxy-backend-instance-.*:.* check' ${_proxy_terminator_config};then
			return 1
		else
			local _endpoints=(`cat ${_proxy_terminator_config} | grep 'server proxy-backend-instance-.*:.* check' | tr -d "\t" | cut -d" " -f3`)
			if [ ${#_endpoints[@]} -lt 1 ];then
				echo "${_banner}: [ERROR] proxy backends are missing from PROXY Terminator configuration file. Maybe not configured for WebPool ?!"
				return 1
			fi
			local _types=""
			for ((i=0;i<${#_endpoints[@]};i++));do
				local _st=`curl --silent -I ${_endpoints[i]} | grep '^Server:' | cut -d" " -f2 | cut -d"/" -f1 | tr '[:upper:]' '[:lower:]'`
				if [ "${_types}" == "" ];then
					if [ "${s}" != "${_st}" ];then
						_types=${_types}" "${_st}
					fi
				else
					for s in ${_types};do
						if [ "${s}" != "${_st}" ];then
							_types=${_types}" "${_st}
						fi
					done
				fi
			done
			local __lod=(${_types})
			if [ ${metric_m2_value} -eq ${#__lod[@]} ];then
				echo "${_banner}: [OK] M2 LoD level (${#__lod[@]}) is equal with the requested probe value (${metric_m2_value})"
				return 0
			else
				echo "${_banner}: [ERROR] M2 LoD level (${#__lod[@]}) is below the requested probe value (${metric_m2_value})"
				return 1
			fi
		fi
	else
		echo "${_banner}: [ERROR] No backend entries could be found in PROXY Terminator configuration file."
		return 1
	fi
}

function webpool_prober_check_metrics() {
	if [ ${metric_m1} -eq 1 ];then
		if ! webpool_prober_metric_m1;then
			_monitoring_msg=`proxy_prober_push_message ${_proxy_prober_monitoring_component} \
														${_proxy_prober_monitoring_object} \
														_proxy_prober_monitoring_labels[@] \
														${_proxy_prober_monitoring_type} \
														_proxy_prober_monitoring_data[@]`
			proxy_prober_push_notification ${_monitoring_msg}
			rm -f ${_monitoring_msg}
			exit 1
		fi
	fi
	if [ ${metric_m2} -eq 1 ];then
		if ! webpool_prober_metric_m2;then
			_monitoring_msg=`proxy_prober_push_message ${_proxy_prober_monitoring_component} \
														${_proxy_prober_monitoring_object} \
														_proxy_prober_monitoring_labels[@] \
														${_proxy_prober_monitoring_type} \
														_proxy_prober_monitoring_data[@]`
			proxy_prober_push_notification ${_monitoring_msg}
			rm -f ${_monitoring_msg}
			exit 1
		fi
	fi
}